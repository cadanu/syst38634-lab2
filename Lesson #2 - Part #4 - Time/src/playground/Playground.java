package playground;

import java.io.Console;

public class Playground {
	
	public static void main(String[] args) {
		getCodePoints();
	}

	public static void calcSmallest() {
		int eval = 0;
		int[] nums = {25, 37, 29};		
		for(int i = 0; i < nums.length; ++i) {
			if(i == 0) eval = nums[i];
			if(nums[i] < eval) eval = nums[i];
		}
		System.out.printf("The smallest number is %d.", eval);
	}
	
	public static void calcAvg() {
		int avg = 0;
		int[] nums = {25, 45, 65};
		for(int i = 0; i < nums.length; ++i) {
			avg = avg + nums[i];
		}
		avg = avg / nums.length;
		System.out.printf("The average of %d, %d and %d is %d.", nums[0], nums[1], nums[2], avg);
	}
	
	public static void getMidChar() {
		String mid = "";
		String str = "350";
		if(str.length() % 2 == 0)
		{
			char[] temp = {( str.charAt(str.length() / 2) ), ( str.charAt((str.length() / 2) + 1) )};
			mid = "" + temp[0] + temp[1];
		}
		else {
			char[] temp = {( str.charAt( (int)(Math.ceil(str.length() / 2) )) )};  
			mid = "" + temp[0];
		}
		System.out.printf("The middle character(s): %s.", mid);		
	}
	
	public static void getVowels() {
		char[] vowels = {'a', 'e', 'i', 'o', 'u'};
		String checkVowels = "w3resource";		
		int inc = 0;
		for(int i = 0; i < checkVowels.length(); ++i) {
			//char getChar = checkVowels.charAt(i);
			
			for(int j = 0; j < vowels.length; ++j) {
				if(checkVowels.charAt(i) == vowels[j]) inc += 1;
			}
		}
		System.out.printf("There are %d vowels in %s.", inc, checkVowels);
	}
	
	public static void getCharAt() {
		int pos = 10;
		String input = "Java Exercises!";
		System.out.printf("The character at position 10 is %c.", input.charAt(pos));
	}
	
	public static void getCodePoints() {
		String checkCP = "w3rsource.com";
		int inc = 0;
		for(int i = 0; i < checkCP.length(); ++i) {
			if(checkCP.codePointAt(i) != 0) inc += 1;
		}
		System.out.printf("The code point count is %d.", inc);
	}

}
