package time;

import static org.junit.Assert.*;

import org.junit.Ignore;
import org.junit.Test;

public class TimeTest {
	
	
	@Ignore @Test
	public void testGetTotalSecondsInit() {		
		fail("Not yet implemented");
	}
	
	@Ignore @Test
	public void testGetTotalSecondsReg() {
		int totalSeconds = Time.getTotalSeconds("01:01:01");// regular	
		assertTrue("The time provided matches the result.", totalSeconds == 3661);
	}
	
	@Ignore @Test(expected=StringIndexOutOfBoundsException.class)
	public void testGetTotalSecondsExcept() {		
		//int totalSecondsExcept = 0;//Time.getTotalSeconds("010101");// exceptional
		assertNull("This will fail due to erroneous input.", Time.getTotalSeconds("010101"));
	}
	
	@Ignore @Test
	public void testGetTotalSecondsBin() {		
		int totalSecondsExcept = Time.getTotalSeconds("01:01:00");// exceptional
		assertTrue("The time provided will be less than total seconds.", totalSecondsExcept < 3661);
	}
	
	@Ignore @Test
	public void testGetTotalSecondsBout() {		
		int totalSecondsExcept = Time.getTotalSeconds("01:01:02");// exceptional
		assertFalse("The time provided will be more than total seconds.", totalSecondsExcept <= 3661);
	}
	
	//---------------------------------------------------------------------------------------------------
	
	@Test
	public void testGetTotalMillisecondsRegular() 
	{
		int milli = Time.getTotalMilliseconds("12:05:05:05");
		assertTrue("milli is equal to 05.", milli==05);
	}
	
	@Test (expected=NumberFormatException.class)
	public void testGetTotalMillisecondsException()
	{
		int milli = Time.getTotalMilliseconds("12:05:05:0555");
		fail("testGetTotalMillisecondsException throws NumberFormatException.");
	}
	
	@Test
	public void testGetTotalMillisecondsBoundaryIn()
	{
		int milli = Time.getTotalMilliseconds("12:05:05:999");
		assertTrue("milli is equal to 999.", true);
	}
	
	@Test (expected=NumberFormatException.class)
	public void testGetTotalMillisecondsBoundaryOut()
	{
		int milli = Time.getTotalMilliseconds("12:05:05:1000");
		fail("milli is equal to 1000");
	}

}
